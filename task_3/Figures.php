<?php

interface Figure {
    public function getArea();
}

define('Pi', 3.1415);
 
class Circle implements Figure 
{
    private $radius;
    
    public function __construct($radius){
        $this->radius = $radius;
    }
    
    public function getArea(){
        return Pi * $this->radius**2;
    }
}
 
class Rectangle implements Figure 
{
    private $a;
    private $b;
    
    public function __construct($a, $b){
        $this->a = $a;
        $this->b = $b;
    }

    public function getArea(){
        return $this->a * $this->b;
    }
}

class Triangle implements Figure 
{
    private $a;
    private $b;
    private $c;
    
    public function __construct($a, $b, $c) {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }
    
    public function getArea(){
        $p = ($this->a + $this->b + $this->c)/2;
        return sqrt($p * ($p - $this->a) * ($p - $this->b) * ($p - $this->c));
    }
}

?>