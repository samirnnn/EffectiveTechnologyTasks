<?php

include_once 'Figures.php';
 
class FigureFactory {
 
    public static function createFigureFromJSON($json_obj) : Figure{
        switch ($json_obj['type']){
            case 'circle':
                return new Circle($json_obj['radius']);
            case 'rectangle':
                return new Rectangle($json_obj['a'], $json_obj['b']);
            case 'triangle':
                return new Triangle($json_obj['a'], $json_obj['b'], $json_obj['c']);
            default :
                throw new Exception('Invalid type.');
        }
    } 
}