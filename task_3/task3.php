<?php

include_once 'FigureFactory.php';
 
function cmp(Figure $figure1, Figure $figure2){
    $area1 = $area1->getArea();
    $area2 = $area2->getArea();
    if ($area1 == $area2) return 0;
    return ($area1 > $area2) ? -1 : 1;
}

$str_json = file_get_contents('figures.json');
$json_data = json_decode($str_json, true);

$figures = [];
foreach($json_data as $json_figure){
    $figures[] = FigureFactory::createFigureFromJSON($json_figure);
}
 
usort($figures, "cmp");  
foreach($figures as $f){
    print_r($f);
    echo "area is: ".$f->getArea()."\n\n";
}