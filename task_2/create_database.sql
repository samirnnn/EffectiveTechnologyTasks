﻿ 
CREATE TABLE author(
	id serial PRIMARY KEY, -- or ISBN
	last_name VARCHAR(64) NOT NULL,
	first_name VARCHAR(64) NOT NULL
);
   
CREATE TABLE book(
	id serial PRIMARY KEY,
	title VARCHAR(64) NOT NULL
	genre VARCHAR(64) NOT NULL, 
	publication_date date NOT NULL,
);

CREATE TABLE author_book(
	author_id int REFERENCES author(id) ON DELETE CASCADE,
	book_id int REFERENCES book(id) ON DELETE CASCADE,
	PRIMARY KEY (author_id, book_id)
);
