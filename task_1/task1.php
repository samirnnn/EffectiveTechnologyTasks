<?php
 
ini_set('memory_limit', '-1');
  
function getFirstPrimesSum($max_number){
    $numbers = array_fill(2, $max_number-1, true);
    for($i = 2; $i**2 <= $max_number; $i++){ 
        if ($numbers[$i]) {
            for ($j = $i**2; $j <= $max_number; $j += $i) { 
                $numbers[$j] = false;
            }
        }
    }

    $sum = 0;
    foreach($numbers as $num => $is_prime){
        if($is_prime){
            $sum += $num;
        }
    }
    echo $sum;
}

getFirstPrimesSum(2000000);
